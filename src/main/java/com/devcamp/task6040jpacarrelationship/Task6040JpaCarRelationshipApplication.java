package com.devcamp.task6040jpacarrelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6040JpaCarRelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task6040JpaCarRelationshipApplication.class, args);
	}

}
