package com.devcamp.task6040jpacarrelationship.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task6040jpacarrelationship.model.CarType;
import com.devcamp.task6040jpacarrelationship.repository.ICarTypeRepository;

@Service
public class CarTypeService {
    @Autowired
    ICarTypeRepository carTypeRepository;
    public List<CarType> getAllCarTypes(){
        List<CarType> typeList = new ArrayList<CarType>();
        carTypeRepository.findAll().forEach(typeList::add);
        return typeList;
    }
}
