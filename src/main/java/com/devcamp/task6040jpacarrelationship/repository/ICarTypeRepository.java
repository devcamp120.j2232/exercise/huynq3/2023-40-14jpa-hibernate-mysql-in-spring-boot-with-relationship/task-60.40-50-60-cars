package com.devcamp.task6040jpacarrelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6040jpacarrelationship.model.CarType;

public interface ICarTypeRepository extends JpaRepository<CarType, Long>{
    
}
