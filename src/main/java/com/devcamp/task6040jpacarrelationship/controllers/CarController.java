package com.devcamp.task6040jpacarrelationship.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6040jpacarrelationship.model.Car;
import com.devcamp.task6040jpacarrelationship.model.CarType;
import com.devcamp.task6040jpacarrelationship.service.CarService;
import com.devcamp.task6040jpacarrelationship.service.CarTypeService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CarController {
    @Autowired
    private CarService carService;
    @Autowired
    private CarTypeService carTypeService;
    //lấy danh sách all cars
    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAllCarApi(){
        try {
            return new ResponseEntity<>(carService.getAllCars(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //lấy danh sách car type theo car code
    @GetMapping("/cartype-query")
    public ResponseEntity<Set<CarType>> getTypesByCarCode(@RequestParam (value= "carCode") String carCode){
        try {
            Set<CarType> carTypes = carService.getCarTypebyCarCode(carCode);
            if (carTypes != null) {
				return new ResponseEntity<>(carTypes, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/all-types")
    public ResponseEntity<List<CarType>> getAllTypeApi(){
        try {
            return new ResponseEntity<>(carTypeService.getAllCarTypes(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
